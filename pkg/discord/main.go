package discord

import (
	"log"
	"os"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/colourdelete/jotai/pkg/jotai"
)

const ID = `discord`

func init() {
	jotai.RegisterPlatform(ID, jotai.Platform{
		HandlerFunc: client,
		Receiver:    nil,
		Sender:      nil,
		Statuser:    nil,
	})
}

func client(wg *sync.WaitGroup, cfg jotai.Config, h jotai.Receiver) {
	defer wg.Done()

	logger := log.New(os.Stderr, "[clients.discord] ", log.Flags())

	// Check config
	if cfg.Token == "" {
		logger.Print(`invalid config: token is blank`)
	}

	if cfg.SigningSecret != "" {
		logger.Print(`invalid config: signing-secret is blank`)
	}

	s, err := discordgo.New("Bot " + cfg.Token)
	if err != nil {
		logger.Printf(`while creating client: %s`, err)
	}

	c := NewDiscordClient(
		s,
		logger,
		cfg,
	)

	c.SetReceiver(h)

	logger.Printf(
		`while running client: %s`,
		c.Run(),
	)
}

func (c *Client) handlerMessageCreate() func(s *discordgo.Session, m *discordgo.MessageCreate) {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			return
		}

		if m.Content == "jotai ping" { // TODO: make it not hardcoded
			_, _ = s.ChannelMessageSend(m.ChannelID, "Pong!")
		}

		parsedTime, err := m.Timestamp.Parse()
		if err != nil {
			parsedTime = time.Now() // substitute it with time.Now(), should be close enough
		}

		_ = c.handler.Handle(jotai.Message{ //nolint:wsl
			From:    ID + `:` + m.Author.ID,
			Content: m.Content,
			Time:    parsedTime,
		})
	}
}

type Client struct {
	client  *discordgo.Session
	handler jotai.Receiver
	logger  *log.Logger
	config  jotai.Config
}

func NewDiscordClient(s *discordgo.Session, logger *log.Logger, config jotai.Config) *Client {
	return &Client{
		client: s,
		logger: logger,
		config: config,
	}
}

func (c *Client) SetReceiver(handler jotai.Receiver) {
	c.handler = handler
	c.client.AddHandler(c.handlerMessageCreate())
}

func (c *Client) Run() error {
	defer c.client.Close()
	err := c.client.Open()
	if err != nil {
		return err
	}
	timer := time.NewTicker(c.config.Heartbeat)
	for {
		<-timer.C
		c.logger.Print("heartbeat")
	}
}
