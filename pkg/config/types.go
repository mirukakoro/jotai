package config

import "fmt"

type Config interface {
	Get(string) (string, bool)
	Set(string, string) error
}

type Map struct {
	data map[string]string
}

type NotFoundError struct {
	s string
}

func (n NotFoundError) Error() string {
	return fmt.Sprintf(`%s not found`, n.s)
}

func (m *Map) Get(s string) (string, error) {
	v, ok := m.data[s]
	if !ok {
		return "", NotFoundError{s: s}
	}
	return v, nil
}

func (m *Map) Set(k, v string) error {
	m.data[k] = v
	return nil
}
