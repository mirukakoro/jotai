package slack

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"gitlab.com/colourdelete/jotai/pkg/jotai"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
)

func client(wg *sync.WaitGroup, cfg jotai.Config, h jotai.Receiver) {
	defer wg.Done()

	logger := log.New(os.Stderr, "[clients.slack] ", log.Flags())
	c := NewSlackClient(
		cfg.SigningSecret,
		slack.New(cfg.Token, slack.OptionDebug(true)),
	)
	c.SetReceiver(h)
	logger.Printf(
		`while running client: %s`,
		c.Run(),
	)
}

func init() {
	jotai.RegisterPlatform("slack", jotai.Platform{
		HandlerFunc: client,
		Receiver:    nil,
		Sender:      nil,
		Statuser:    nil,
	})
}

type Client struct {
	client        *slack.Client
	handler       jotai.Receiver
	signingSecret string
}

func NewSlackClient(signingSecret string, client *slack.Client) *Client {
	return &Client{
		client:        client,
		signingSecret: signingSecret,
	}
}

func (s *Client) SetReceiver(handler jotai.Receiver) {
	s.handler = handler
}

func (s *Client) Run() error {
	if s.signingSecret == "" {
		return errors.New("signing-secret is blank")
	}
	attachment := slack.Attachment{
		Pretext: "some pretext",
		Text:    "some text",
		// Uncomment the following part to send a field too
		Fields: []slack.AttachmentField{
			slack.AttachmentField{
				Title: "a",
				Value: "no",
			},
		},
	}

	channelID, timestamp, err := s.client.PostMessage(
		"CHANNEL_ID",
		slack.MsgOptionText("Some text", false),
		slack.MsgOptionAttachments(attachment),
		slack.MsgOptionAsUser(false),
	)
	if err != nil {
		return err
	}
	fmt.Printf("Message successfully sent to channel %s at %s", channelID, timestamp)

	http.HandleFunc("/events-endpoint", func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		sv, err := slack.NewSecretsVerifier(r.Header, s.signingSecret)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if _, err := sv.Write(body); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := sv.Ensure(); err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		eventsAPIEvent, err := slackevents.ParseEvent(body, slackevents.OptionNoVerifyToken())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if eventsAPIEvent.Type == slackevents.URLVerification {
			var r *slackevents.ChallengeResponse
			err := json.Unmarshal(body, &r)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "text")
			_, err = w.Write([]byte(r.Challenge))
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		if eventsAPIEvent.Type == slackevents.CallbackEvent {
			innerEvent := eventsAPIEvent.InnerEvent
			if ev, ok := innerEvent.Data.(*slackevents.AppMentionEvent); ok {
				_, _, err := s.client.PostMessage(ev.Channel, slack.MsgOptionText("Yes, hello.", false))
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
			}
		}
	})

	return http.ListenAndServe(":8081", nil)
}
