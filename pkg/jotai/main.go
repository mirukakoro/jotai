package jotai

import (
	"log"
	"sync"
	"time"
)

type Config struct {
	Token         string        `toml:"token"`
	SigningSecret string        `toml:"signing-secret"`
	Heartbeat     time.Duration `toml:"heartbeat"`
}

type HandlerFunc func(*sync.WaitGroup, Config, Receiver)

var clients = make(map[string]HandlerFunc)

func RegisterClient(name string, f HandlerFunc) {
	clients[name] = f
}

func Clients() map[string]HandlerFunc {
	return clients
}

var platforms = make(map[string]Platform)

func RegisterPlatform(name string, p Platform) {
	platforms[name] = p
}

func Platforms() map[string]Platform {
	return platforms
}

type Platform struct {
	HandlerFunc
	Receiver
	Sender
	Statuser
}

type Client interface {
	SetReceiver(Receiver)
	Run() error
}

type Receiver interface {
	Handle(Message) error
}

type Sender interface {
	Send(Message) error
}

type Message struct {
	From    string // only receiving
	To      string // only sending
	Content string
	Time    time.Time // only receivng
}

// Statuser is a interface for platforms that support setting the status of the user.
// Some platforms do not have a online/offline feature.
// When this is the case, online/offline should take precedence if not nil.
type Statuser interface {
	SetStatus(online bool, icon rune, msg string)
}

// DebugHandler is only for debugging purposes. It logs the messages onto logger.
type DebugHandler struct {
	logger *log.Logger
}

// NewDebugHandler creates a *DebugHandler.
func NewDebugHandler(logger *log.Logger) *DebugHandler {
	return &DebugHandler{logger: logger}
}

// Handle implements Handler. It logs the messages to DebugHandler.logger
func (d *DebugHandler) Handle(m Message) error {
	d.logger.Printf("@ %s, from %s: %s", m.Time, m.From, m.Content)
	return nil
}
