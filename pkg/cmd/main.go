package cmd

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

type Subcommands struct {
	subcommands      map[string]*flag.FlagSet
	handlers         map[string]func()
	subcommandsSlice []string
}

var defaultSubcommands = NewSubcommands()

func NewSubcommands() *Subcommands {
	return &Subcommands{
		subcommands:      map[string]*flag.FlagSet{},
		subcommandsSlice: []string{},
		handlers:         map[string]func(){},
	}
}

func RegisterSubcommand(flagSet *flag.FlagSet, handler func()) {
	defaultSubcommands.RegisterSubcommand(flagSet, handler)
}

func Parse(args []string) (func(), error) {
	return defaultSubcommands.Parse(args)
}

func (s *Subcommands) RegisterSubcommand(flagSet *flag.FlagSet, handler func()) {
	s.subcommands[flagSet.Name()] = flagSet
	s.subcommandsSlice = append(s.subcommandsSlice, flagSet.Name())
	s.handlers[flagSet.Name()] = handler
}

func (s *Subcommands) Parse(args []string) (func(), error) {
	if len(os.Args) < 2 {
		return func() {}, fmt.Errorf(`expected subcommands %s`, strings.Join(s.subcommandsSlice, " or "))
	}

	subcommand := os.Args[1]
	flagSet, ok := s.subcommands[subcommand]

	if !ok {
		return func() {}, fmt.Errorf(`expected subcommands %s`, strings.Join(s.subcommandsSlice, " or "))
	}

	err := flagSet.Parse(os.Args[2:])
	if err != nil {
		return func() {}, err
	}

	return s.handlers[subcommand], nil
}
