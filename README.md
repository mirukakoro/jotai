# Jotai

Jotai is a program to manage all your statuses in one place.

You can set up your own instance of Jotai with custom settings like platforms and replies.

For more info, see the GitBook at https://colourdelete.gitlab.io/jotai
